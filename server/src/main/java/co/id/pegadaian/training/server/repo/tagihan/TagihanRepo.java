package co.id.pegadaian.training.server.repo.tagihan;

import co.id.pegadaian.training.server.entity.Tagihan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface TagihanRepo extends JpaRepository<Tagihan, Integer> {

    Tagihan findByNomortagihan(String nomortagihan);

    @Modifying
    @Transactional
    @Query(value = "UPDATE tagihan SET flag = ?1 WHERE nomortagihan = ?2", nativeQuery = true)
    void updateByNomortagihan(Boolean flag, String nomortagihan);

}
