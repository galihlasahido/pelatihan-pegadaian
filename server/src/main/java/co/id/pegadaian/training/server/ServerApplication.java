package co.id.pegadaian.training.server;

import co.id.pegadaian.training.server.repo.tagihan.TagihanService;
import org.jpos.util.NameRegistrar;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = "file:./config/application.properties")
public class ServerApplication {

	public static ApplicationContext ctx;

	public static void main(String[] args) {
		ctx = SpringApplication.run(ServerApplication.class, args);
		TagihanService tagihanImpl = (TagihanService) ctx.getBean("tagihanImpl");
		NameRegistrar.register("tagihanService", tagihanImpl);

	}
}
