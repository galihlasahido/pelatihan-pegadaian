package co.id.pegadaian.training.server.repo.tagihan;

import co.id.pegadaian.training.server.entity.Tagihan;
import org.springframework.stereotype.Component;

@Component
public interface TagihanService {
    Tagihan findByNomorTagihan(String nomortagihan);
    void updateNomorTagihan(String nomortagihan);
}
