package co.id.pegadaian.training.server.repo.tagihan;

import co.id.pegadaian.training.server.entity.Tagihan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TagihanImpl implements TagihanService {

    @Autowired(required = false)
    private TagihanRepo repo;

    @Override
    public Tagihan findByNomorTagihan(String nomortagihan) {
        return repo.findByNomortagihan(nomortagihan);
    }

    @Override
    public void updateNomorTagihan(String nomortagihan) {
        repo.updateByNomortagihan(true, nomortagihan);
    }
}
