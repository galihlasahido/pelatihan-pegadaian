package co.id.pegadaian.training.server.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "co.id.pegadaian.training.server.repo")
@EntityScan(basePackages="co.id.pegadaian.training.server.entity")
public class DBConfig {
}
