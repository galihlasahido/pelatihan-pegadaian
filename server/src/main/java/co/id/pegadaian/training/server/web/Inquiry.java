package co.id.pegadaian.training.server.web;

import co.id.pegadaian.training.server.entity.Tagihan;
import co.id.pegadaian.training.server.repo.tagihan.TagihanService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class Inquiry {

    @Autowired
    private TagihanService service;

    @PostMapping(value = "/inquiry", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Map<String, Object> inquiry(@RequestBody String json) throws IOException {
        Map<String, Object> maps = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map;
        map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
        Tagihan result = service.findByNomorTagihan(String.valueOf(map.get("nomortagihan")));
        if(result!=null) {
            maps.put("61", result.getNama());
            maps.put("4", result.getNominal());
            maps.put("48", result.getNomortagihan());
            maps.put("39", "00");
            maps.put("desc", "success");
        } else {
            maps.put("39", "01");
            maps.put("desc", "failed");
        }
        return maps;
    }
}
