package co.id.pegadaian.training.server.web;

import co.id.pegadaian.training.server.repo.tagihan.TagihanService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class Payment {

    @Autowired
    private TagihanService service;

    @PostMapping(value = "/payment", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Map<String, Object> payment(@RequestBody String json) throws IOException {
        Map<String, Object> maps = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> map;
        map = mapper.readValue(json, new TypeReference<Map<String, String>>(){});
        service.updateNomorTagihan(String.valueOf(map.get("nomortagihan")));
        maps.put("rc", "00");
        maps.put("desc", "success");
        return maps;
    }
}
