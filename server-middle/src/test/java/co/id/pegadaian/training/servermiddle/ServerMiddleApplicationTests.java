package co.id.pegadaian.training.servermiddle;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServerMiddleApplicationTests {

	@Test
	public void contextLoads() {
	}

}
