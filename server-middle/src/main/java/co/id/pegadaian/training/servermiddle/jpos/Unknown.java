package co.id.pegadaian.training.servermiddle.jpos;

import org.jpos.core.Configurable;
import org.jpos.core.Configuration;
import org.jpos.core.ConfigurationException;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOSource;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;

import java.io.IOException;
import java.io.Serializable;

public class Unknown implements TransactionParticipant, Configurable {
    private Configuration cfg;
    private String isorequest;
    private String isosource;
    private String appname;
    private String mux;
    private String logger;
    private Long timeout;

    @Override
    public void setConfiguration(Configuration configuration) throws ConfigurationException {
        this.cfg = configuration;
        this.isorequest = cfg.get("isorequest");
        this.isosource = cfg.get("isosource");
        this.logger = cfg.get("logger");
        this.appname = cfg.get("appname");
        this.mux = cfg.get("mux");
        this.timeout = cfg.getLong("timeout");
    }

    @Override
    public int prepare(long l, Serializable serializable) {
        return PREPARED | READONLY;
    }

    @Override
    public void commit(long l, Serializable serializable) {

        ISOMsg message = (ISOMsg)((Context) serializable).get(this.isorequest);
        ISOSource source = (ISOSource)((Context) serializable).get(this.isosource);

        try {
            message.set(39,"x1");
            message.setResponseMTI();
            source.send(message);

        } catch (ISOException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void abort(long l, Serializable serializable) {

    }

}
