package co.id.pegadaian.training.servermiddle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = "file:./config/application.properties")
public class ServerMiddleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerMiddleApplication.class, args);
	}
}
